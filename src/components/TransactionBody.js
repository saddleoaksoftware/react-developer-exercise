import React from "react";

export function TransactionBody(props) {
  return (
    <>
      <table>
        <tbody>
          {props.lines.map((line) => (
            <tr key={line.description}>
              <td>{line.description}</td>
              <td>{line.amount}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
