import React from "react";

export function TransactionFooter(props) {
  return (
    <>
      <hr />
      <h2>Total for this transaction: {props.total}</h2>
    </>
  );
}
