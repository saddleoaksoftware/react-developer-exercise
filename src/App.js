import React from "react";

import { ItemReceipt } from "views/ItemReceipt";

function App() {
  return <ItemReceipt />;
}

export default App;
