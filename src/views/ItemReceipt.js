import React, { useState } from "react";

import { TransactionHeader } from "components/TransactionHeader";
import { TransactionBody } from "components/TransactionBody";
import { TransactionFooter } from "components/TransactionFooter";

export function ItemReceipt(props) {
  const [lines, setLines] = useState([]);

  const [values, setValues] = useState({ description: "", amount: 0 });

  function linesTotal(lines) {
    console.log("linesTotal, lines: %o", lines);
    var total = 0;
    for (var i = 0; i < lines.length; i++) {
      total = total + lines[i].amount;
    }
    return total;
  }

  function onChange(e) {
    var newValues = { ...values, [e.target.id]: e.target.value };
    setValues(newValues);
  }

  function addItem() {
    var newLines = [...lines];
    newLines.push({ description: values.description, amount: values.amount });
    setLines(newLines);
  }

  return (
    <>
      <TransactionHeader>Item receipt</TransactionHeader>
      <TransactionBody lines={lines} />
      <TransactionFooter total={linesTotal(lines)} />
      <hr />
      <form style={{ width: "25em" }} onSubmit={(e) => e.preventDefault()}>
        <fieldset>
          <legend>Add a line item</legend>
          <p>
            <label htmlFor="description">Description </label>
            <input
              id="description"
              value={values.description}
              onChange={onChange}
            />
          </p>
          <p>
            <label htmlFor="amount">Amount </label>
            <input id="amount" value={values.amount} onChange={onChange} />
          </p>
          <p>
            <button onClick={addItem}>Add</button>
          </p>
        </fieldset>
      </form>
    </>
  );
}
